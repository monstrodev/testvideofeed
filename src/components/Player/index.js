import React, { useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setActiveStream } from '../../redux/actions';

export default ({stream}) => {
  const activeStreamId = useSelector(state => state.main.activeStreamId);
  const dispatch = useDispatch();
  const [isPlayButtonShown, setPlayButtonShown] = useState(true);
  const videoNode = useRef(null);

  useEffect(() => {
    if (activeStreamId && activeStreamId !== stream.id) {
      videoNode.current.pause();
    }
  }, [activeStreamId, stream.id]);

  const handleClickButton = () => {
    dispatch(setActiveStream(stream.id));
    isPlayButtonShown ? videoNode.current.play() : videoNode.current.pause();
    setPlayButtonShown(!isPlayButtonShown);
  };

  const isStreamPaused = !isPlayButtonShown && activeStreamId === stream.id;

  return (
    <div className="c-player">
      <h4 className="c-player__header">Stream id: {stream.id}</h4>
      <div className="c-player__content">
        <video
          ref={videoNode}
          width="270"
          controls={false}
          autoPlay={true}
          loop
          muted={true}
        >
          <source src={stream.source} type="video/mp4" />
        </video>
        <div onClick={() => handleClickButton()} className="c-player__actions">
          <div className={`c-player__action c-player__action--${isStreamPaused ? 'paused' : 'active'}`}/>
        </div>
      </div>
    </div>
  )
}