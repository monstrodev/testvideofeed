import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Grid, Row } from 'react-flexbox-grid';

import Player from '../components/Player';
import { setActiveStream } from '../redux/actions';

function App() {
  const streamMatrix = useSelector(state => state.main.streams);
  const dispatch = useDispatch();

  useEffect(() => {
    return () => dispatch(setActiveStream(null))
  }, [dispatch]);

  return (
    <Grid fluid>
      {streamMatrix.map((row, index) =>
        <Row key={`row-${index}`} center="xs">
          {row.map(stream => <Player key={`stream-${stream.id}`} stream={stream} />)}
        </Row>
      )}
    </Grid>
  );
}

export default App;