const SET_ACTIVE_STREAM = 'SET_ACTIVE_STREAM';

export const setActiveStream = (id = null) => {
  return {
    type: SET_ACTIVE_STREAM,
    data: id
  }
};