import { combineReducers } from 'redux';

const initialState = {
  activeStreamId: null,
  streams: []
};

const source = 'https://def-sec-intel.s3.eu-central-1.amazonaws.com/res_c2.mp4';
const columnsCount = 5;
const rowsCount = 4;

function reducer() {
  let id = 0;
  for (let i = 1; i <= rowsCount; i++) {
    const row = [];
    for (let j = 1; j <= columnsCount; j++) {
      row.push({ id, source });
      id++;
    }
    initialState.streams.push(row);
  }

  return function(state = initialState, action) {
    switch (action.type) {
      case 'SET_ACTIVE_STREAM': {
        return {
          ...state,
          activeStreamId: action.data
        }
      }
      default: return state
    }
  }
}

export default function() {
  const appReducer = combineReducers({
    main: reducer(),
  });

  return (state, action) => appReducer(state, action);
}